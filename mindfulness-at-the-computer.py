#!/usr/bin/env python3
import logging
import logging.handlers
import os
import sqlite3
import sys
from PyQt5 import QtCore
from PyQt5 import QtWidgets
import mc.gui.settings_win
from mc import mc_global
import mc.db

# The following import looks like it isn't used, but it is necessary for importing the images.
# noinspection PyUnresolvedReferences
import icons.matc_rc # pylint: disable=unused-import

LOG_FILE_NAME_STR = "matc.log"


if __name__ == "__main__":
    mc_global.db_file_exists_at_application_startup_bl = os.path.isfile(mc_global.get_database_filename())
    # -settings this variable before the file has been created

    logger = logging.getLogger()
    # -if we set a name here for the logger the file handler will no longer work, unknown why
    logger.handlers = []  # -removing the default stream handler first
    # logger.propagate = False
    rfile_handler = logging.handlers.RotatingFileHandler(LOG_FILE_NAME_STR, maxBytes=8192, backupCount=2)
    rfile_handler.setLevel(logging.WARNING)
    # formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
    rfile_handler.setFormatter(formatter)
    logger.addHandler(rfile_handler)
    stream_handler = logging.StreamHandler()
    stream_handler.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(levelname)s: %(message)s')
    stream_handler.setFormatter(formatter)
    logger.addHandler(stream_handler)

    matc_qapplication = QtWidgets.QApplication(sys.argv)

    # Application information
    mc.mc_global.sys_info_telist.append(("Application name", mc.mc_global.APPLICATION_TITLE_STR))
    mc.mc_global.sys_info_telist.append(("Application version", mc.mc_global.APPLICATION_VERSION_STR))
    db_conn = mc.db.Helper.get_db_connection()
    mc.mc_global.sys_info_telist.append(("Application database schema version", mc.db.get_schema_version(db_conn)))
    mc.mc_global.sys_info_telist.append(("Python version", sys.version))
    mc.mc_global.sys_info_telist.append(("SQLite version", sqlite3.sqlite_version))
    mc.mc_global.sys_info_telist.append(("PySQLite (Python module) version", sqlite3.version))
    mc.mc_global.sys_info_telist.append(("Qt version", QtCore.qVersion()))
    # noinspection PyUnresolvedReferences
    mc.mc_global.sys_info_telist.append(("PyQt (Python module) version", QtCore.PYQT_VERSION_STR))

    # set stylesheet
    stream = QtCore.QFile(os.path.join(mc.mc_global.get_base_dir(), "matc.qss"))
    stream.open(QtCore.QIODevice.ReadOnly)
    matc_qapplication.setStyleSheet(QtCore.QTextStream(stream).readAll())

    desktop_widget = matc_qapplication.desktop()
    mc.mc_global.sys_info_telist.append(("Virtual desktop", str(desktop_widget.isVirtualDesktop())))
    mc.mc_global.sys_info_telist.append(("Screen count", str(desktop_widget.screenCount())))
    mc.mc_global.sys_info_telist.append(("Primary screen", str(desktop_widget.primaryScreen())))

    translator = QtCore.QTranslator()
    # Warning While removing debug keep the loading call intact
    system_locale = QtCore.QLocale.system().name()
    logging.info('System Localization: ' + system_locale)
    logging.info(
        'Localization Load Status: ' + str(translator.load(system_locale + '.qm', 'translate/' + system_locale))
    )  # -name, dir
    matc_qapplication.installTranslator(translator)

    matc_qapplication.setQuitOnLastWindowClosed(False)
    matc_main_window = mc.gui.settings_win.SettingsWin()

    if mc.mc_global.db_upgrade_message_str:
        # noinspection PyCallByClass
        QtWidgets.QMessageBox.warning(matc_main_window, "title", mc.mc_global.db_upgrade_message_str)

    sys.exit(matc_qapplication.exec_())
